'use strict'
var React = require('react');
var TodoStore = require('../stores/TodoStore.js');
var AppConstants = require('../constants/AppConstants.js');
var actions = require('../actions/AppActionCreator.js');
var shortId = require('shortid');


module.exports  = React.createClass({

    doclick: function(){

        if(this.props.sidestatus)
            actions.sidebaropen(false);
        else
            actions.sidebaropen(true);
    },
     render: function() {



         var classes ="appbar ";

         if(this.props.sidestatus)
         {
             classes += "sideon";
         }

        return (
            <div className={classes} >
                <i className="fa fa-bars fa-2x"
                    onClick={this.doclick}></i>
                <span className="fa fa-2x">  Title</span>
            </div>
        );
     },


});
