'use strict'
var React = require('react');
var TodoStore = require('../stores/TodoStore.js');
var AppConstants = require('../constants/AppConstants.js');
var Sidebar = require('./sidebar.jsx');
var Appbar = require('./appbar.jsx');
var idResize;

var MainApp = React.createClass({

    /**
     * component API,mount this.state
     */
    getInitialState: function() {
        console.log("--------------Main App getInitialState-----------------");
        var o = this.getTruth();
        o.screenSize = 'desktop';
        console.log(o);
        return o;
    },
    componentWillMount: function() {
          TodoStore.addListener( AppConstants.CHANGE_EVENT, this._onChange );
          window.addEventListener('resize', this.handleResize );
          this.handleResize();
    },
    handleResize: function(evt){

      idResize = setTimeout(function(){

          var body = document.body;
          var size;
          // @todo: 改回 1024
          if(body.scrollWidth > 978){
              size = 'desktop';
          }else if(body.scrollWidth > 480){
              size = 'tablet';
          }else{
              size = 'phone';
          }
          // console.log( 'resize: ', body.scrollWidth, body.scrollHeight, ' >size: ', size );
          this.setState({screenSize: size});
      }.bind(this), 0)
    },

    /**
     *
     */
    componentDidMount: function() {
  },
    //========================================================================
    //
    // unmount

    /**
     *
     */
    componentWillUnmount: function() {
        TodoStore.removeChangeListener( this._onChange );
    },
    /**
     *
     */
    componentDidUnmount: function() {
        //
    },

    //========================================================================
    //
    // update
    /**
     *
     */
    componentWillReceiveProps: function(nextProps) {
        //
    },
    /**
     *
     */
    shouldComponentUpdate: function(nextProps, nextState) {
        return true;
    },
    componentWillUpdate: function(nextProps, nextState) {
    },
    /**
     *
     */
    componentDidUpdate: function(prevProps, prevState) {
    },

    //========================================================================
    //
    // render

    /**
     *
     */
     openSidebar: function (){
         console.log("test");
     },
     render: function() {

       return (
         <div>
             <Appbar sidestatus={this.state.siedbarStatus} />
             <Sidebar  on={this.state.siedbarStatus} />
         </div>
       );
     },

    //========================================================================
    //
    // private methods

    /**
     * controller-view model change
     * private method model
     * component life cycle setState()
     * child components
     */
    _onChange: function(){
        //root view sub-view
        this.setState( this.getTruth() );
    },
    getTruth: function() {
        // TodoStore (as the single source of truth)
        return TodoStore.getAll();
    },
    handleClick: function(evt){
    }

});

module.exports = MainApp;
