/**
 * TodoStore
 */

//========================================================================
//
// IMPORT

var shortId = require('shortid');
var AppDispatcher = require('../dispatcher/AppDispatcher');
var AppConstants = require('../constants/AppConstants');
var actions = require('../actions/AppActionCreator');
var EventEmitter = require('events').EventEmitter; //pub/sub

//========================================================================
//
// Private vars

// TodoStore extends EventEmitter
//
// TodoStore public methods
var Store = new EventEmitter();

/*
 *  Status
 */
//========================================================================
var status;
// data of Source Selector
var listSource =
[
    {name:'All Sources',uid:shortId.generate()},
    {name:'Facebook',uid:shortId.generate()},
    {name:'Youtube',uid:shortId.generate()},
    {name:'PTT',uid:shortId.generate()}
];

var siedbarStatus = false;

// data of word Selector
var listWord = [];


//========================================================================
//
// Public API

/**
 * Store class EventEMitter
 */
$.extend( Store, {

    getAll: function(){
        return {
            listWord : listWord,
            listSource: listSource,
            siedbarStatus: siedbarStatus
        };
    },

    //
    noop: function(){}
});
//========================================================================
//
// event handlers

/**
 * Dispatcher
 * dispatchToken  async
 */
Store.dispatchToken = AppDispatcher.register( function eventHandlers(evt){

    // evt .action  view
    //  actionType
    var action = evt.action;
    switch (action.actionType) {

       /**
         *
         */
       case AppConstants.TODO_CREATE:

            listWord.push( action.item );
            $("#sel_search").val("");
            Store.emit( AppConstants.CHANGE_EVENT );
            break;
        case AppConstants.TODO_SIDEOPEN:

                console.log("TODO_SIDEOPEN :",action.item);
             siedbarStatus = action.item;
             Store.emit( AppConstants.CHANGE_EVENT );
             break;


        default:
    }

});

module.exports = Store;
