WayKing 賽道王 2015 UI
===================================

Edit by Charlie
- - -


1.檔案結構
---------------

WayKing2015_UI

+ assets
    + congfig.rb ( for sass used 不用理它 )
    + sass
        + style.scss ( 自訂樣式scss檔,你應該編輯這個檔案 )
    + stylesheets
        + style.css  ( 自訂樣式css檔,利用compass自動生成 )
+ js
    + boot.js ( React 程式進入點 )
    + package.json ( webpack 設定檔案 )
    + webpack.config.js ( webpack 設定檔案 )
    + build
        + bundle.js ( Wabpack將所有東西打包過後,生成的唯一js執行檔 )
    + action
        + AppActionCreator.js ( React action )
    + constants
        + AppConstants.js ( React constants )
    + dispatcher
        + AppDispatcher.js ( React dispatcher )
    + stores
        + TodoStore.js ( React store 所有資料儲存的地方 )
    + views
        + MainApp.jsx ( React views MainApp為最外層的React元件 )
+ intex.html (首頁html檔案)
+ README.md (你現在正在閱讀的說明檔案)
